# Package app into docker image role

Clone a repo and package it in docker image.

## Requirements
- Ansible
- Buildpack
- Docker
- Pip package: docker in order to build the image.

## Role Variables
| Name | Description | Type | Default | Required | 
|------|-------------|------|---------|:-----:|
| app_repo | The git repo url where the app can be found | `string` | `` | yes |
| app_repo_username | The git repo username | `string` | `` | no |
| app_repo_password | The git repo password | `string` | `` | no |
| app_branch | The git repo branch | `string` | `master` | no |
| app_docker_registry_url | The docker registry url | `string` | `https://index.docker.io/v2/` | no |
| secret_app_docker_registry_username | The docker registry username (required if should_push_docker_image is True or if dockerfile base image require auth ) | `string` | `` | no |
| secret_app_docker_registry_password | The docker registry password (required if should_push_docker_image is True or if dockerfile base image require auth ) | `string` | `` | no |
| secret_app_docker_registry_email | The docker registry email | `string` | `` | no |
| app_dockerfile_name | The dockerfile name used for building the docker image | `string` | `Dockerfile` | no |
| app_docker_image_name | The name of builded image | `string` | `myapp` | no |
| app_docker_image_tag | The name of builded tag | `string` | `1.0.0` | no |
| app_docker_build_args | The docker build args used building the docker image | `object` | `{}` | no |
| should_push_docker_image | Determine if the image should be pushed to a registry | `boolean` | `no` | no |
| app_workdir | Tmp directory for the run | `string` | `/tmp/build/app` | no |
| app_directory | Directory where app will be cloned | `string` | `/tmp/build/app/app-repo` | no |
| keltio_builder | The buildpack builder used for building the docker image | `string` | `cnbs/keltio-builder:18` | no |
