FROM node:12.2.0 as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build -- --mode $node_env


FROM nginx:1.13.12-alpine as production-stage
RUN mkdir -p /usr/share/nginx/html
RUN mkdir -p /usr/share/nginx/html/acme

COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY vhost.conf /etc/nginx/conf.d/
COPY mime.types /etc/nginx/mime.types

RUN rm /etc/nginx/conf.d/default.conf

# Create file for acme challenge
RUN mkdir -p /usr/share/nginx/acme
RUN echo "acme-test" > /usr/share/nginx/acme/index.html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
